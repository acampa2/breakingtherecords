import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the breakingRecords function below.
    static int[] breakingRecords(int[] scores) {
        int lowestScore=scores[0],highestScore=scores[0],ls=0,hs=0;
        int[] results= new int [2];

        for(int i=0;i<scores.length;i++){
        if(lowestScore>scores[i]){
                lowestScore=scores[i];
                ls++;
            }
            else if(highestScore<scores[i]){
                highestScore=scores[i];
                hs++;
            }
        }
        results[0]=hs;
        results[1]=ls;
        return results;

    }


    public static void main(String[] args) throws IOException {

        //int[] scores = {10,5,20,20,4,5,2,25,1};
        int[] scores = {3,4,21,36,10,28,35,5,24,42};

        int[] result = breakingRecords(scores);

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);

        }

    }
}
